/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void genericThread(int *number) {
    printf("Thread %d in elaborazione...\n", *number);
    sleep(5);
    printf("Thread %d terminato\n", *number);
}

int main(int argc, char const *argv[]) {
    pthread_t t1, t2;
    int value1 = 1, value2 = 2;
    puts("Creazione del primo thread");
    pthread_create(&t1, NULL, &genericThread, &value1);
    puts("Creazione del secondo thread");
    pthread_create(&t2, NULL, &genericThread, &value2);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    puts("Main exit");
    return 0;
}
