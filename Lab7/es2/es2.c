/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void rilevaCasa(char *string) {
    while (1) {
        if (strcmp(string, "casa") == 0) {
            puts("Casa rilevata");
        }
        if (strcmp(string, "house") == 0) {
            puts("Casa rilevata");
        }
        if (strcmp(string, "exit") == 0) {
            _exit(0);
        }
        sleep(1);
    }
}

int main(int argc, char const *argv[]) {
    char string[21];
    pthread_t t1;
    pthread_create(&t1, NULL, &rilevaCasa, string);

    while (1) {
        printf("Inserisci una stringa: ");
        scanf("%s", string);
    }

    return 0;
}
