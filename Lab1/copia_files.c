/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

unsigned int MAX_LENGHT = 500;
ssize_t BLOCK_SIZE      = 256;

void minCopy(char **dest, const char *src) {
    unsigned int lenght = strlen(src);
    *dest               = calloc(lenght + 1, sizeof(**dest));
    strcpy(*dest, src);
}

// Return number of blocks readed
unsigned int copyFiles(char *input, char *output, ssize_t blockSize) {
    char *stream      = calloc(blockSize, sizeof(*stream));
    ssize_t totalRead = 0;

    // Open file descriptors
    mode_t mode        = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    int inputFiledesc  = open(input, O_RDONLY);
    int outputFiledesc = open(output, O_WRONLY | O_CREAT | O_TRUNC, mode);

    ssize_t readedBites = 1;

    while (readedBites != 0) {
        readedBites = read(inputFiledesc, stream, blockSize);
        write(outputFiledesc, stream, readedBites);
        totalRead++;
    }

    free(stream);
    return totalRead;
}

int main(int argc, char const *argv[]) {
    char *input, *output;
    minCopy(&input, argv[1]);
    minCopy(&output, argv[2]);

    ssize_t bs;
    if (argc >= 4) {
        sscanf(argv[3], "%d", &bs);
    } else {
        bs = BLOCK_SIZE;
    }

    printf("Input file it's: %s\nOutput file it's: %s\nBlock size it's %d byte(s)\n", input, output, bs);
    ssize_t readed = copyFiles(input, output, bs);
    printf("Number of block(s) readed: %d\n", readed);

    free(input);
    free(output);
    return 0;
}
