/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Array.h/ArraysLib.h"
#include "aggregateStructure.h"
#include <pthread.h>

ArraysLib ARRAYS;
unsigned int THREADS = 1;

void valid(aggregateStructure a) {
    a->Valid = *a->Value % a->Dividend == 0;
    a->Ended = true;
}

aggregateStructure getAggregateFromArray(Array a, uint index) {
    return (aggregateStructure)(a->Items[index]);
}

int parseArgs(char const *argv[]) {
    THREADS = atoi(argv[1]);
    return atoi(argv[2]);
}

int main(int argc, char const *argv[]) {
    ARRAYS      = newArraysLib();
    int value   = parseArgs(argv);
    int limit   = value / 2;
    int checkOn = 1;

    Array threadParameters = ARRAYS.new(&free, NULL);
    ARRAYS.allocate(threadParameters, THREADS);
    aggregateStructure tmp;
    for (size_t i = 0; i < THREADS; i++) {
        ARRAYS.set(threadParameters, i, newAggregateStructure(&value));
    }

    while (checkOn <= limit) {
        for (size_t i = 0; i < THREADS; i++) {
            tmp = getAggregateFromArray(threadParameters, i);
            if (tmp->Ended) {
                if (tmp->Dividend != 0) {
                    pthread_join(tmp->ThreadPID, NULL);
                    if (tmp->Valid) {
                        printf("[THREAD %d] %d it's valid\n", i, tmp->Dividend);
                    }
                }
                tmp->Dividend = checkOn++;
                tmp->Ended    = false;
                pthread_create(&tmp->ThreadPID, NULL, (void *)&valid, tmp);
            }
        }
    }

    for (size_t i = 0; i < THREADS; i++) {
        tmp = getAggregateFromArray(threadParameters, i);
        pthread_join(tmp->ThreadPID, NULL);
        if (tmp->Ended) {
            if (tmp->Dividend != 0) {
                if (tmp->Valid) {
                    printf("[THREAD %d] %d it's valid\n", i, tmp->Dividend);
                }
            }
        }
    }

    ARRAYS.free(threadParameters, true);
    return 0;
}
