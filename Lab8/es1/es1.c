/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Array.h/ArraysLib.h"
#include <limits.h>
#include <pthread.h>
#define MAX_CHAR 2000
ArraysLib ARRAYS;
bool MULTITHREAD = false;

typedef struct parameter {
    char *In;
    char *Out;
} * Parameter;

Parameter newParameter(const char *In, const char *Out) {
    Parameter p = malloc(sizeof(*p));
    p->In       = calloc(strlen(In) + 1, sizeof(*p->In));
    p->Out      = calloc(strlen(Out) + 1, sizeof(*p->Out));

    strcpy(p->In, In);
    strcpy(p->Out, Out);
    return p;
}

void freeParameter(Parameter p) {
    free(p->In);
    free(p->Out);
    free(p);
}

Array parseParameters(int argc, char const *argv[]) {
    puts(argv[argc - 1]);
    if (strcmp("-m", argv[argc - 1]) == 0) {
        MULTITHREAD = true;
        argc--;
        puts(argv[argc - 1]);
    }

    Array a = ARRAYS.new(&freeParameter, NULL);
    ARRAYS.allocate(a, (argc--) / 2);

    Parameter p;
    for (size_t i = 1; i < argc; i += 2) {
        p = newParameter(argv[i], argv[i + 1]);
        ARRAYS.add(a, p);
    }

    return a;
}

/**
 * Return index of max value
 */
int getMax(int *a, int n) {
    int max    = INT_MIN;
    uint index = 0;

    for (size_t i = 0; i < n; i++) {
        if (a[i] > max) {
            max   = a[i];
            index = i;
        }
    }

    return index;
}

/**
 * Stupid sort array
 */
void sorting(int *a, int n) {
    uint index;
    int tmp;

    for (size_t i = n; i > 0; i--) {
        index    = getMax(a, i);
        tmp      = a[index];
        a[index] = a[i - 1];
        a[i - 1] = tmp;
    }
}

/**
 * swap two int values
 */
// void swap(int *a, int *b) {
//     int tmp = *a;
//     *a      = *b;
//     *b      = tmp;
// }

/**
 * quicksort wrapper
 */
// void quickSort(int *a, uint n) {
//     if (n == 1 || n == 0) {
//         return;
//     }

//     uint pivot, i, j;
//     pivot = n - 1;
//     i     = 0;
//     j     = pivot - 1;

//     while (true) {
//         while (i < pivot) {
//             if (a[i] > a[pivot]) {
//                 break;
//             }
//         }
//         while (j > 0) {
//             if (a[j] < a[pivot]) {
//                 break;
//             }
//         }
//         if (j > i) {
//             swap(&a[i], &a[j]);
//         } else {
//             break;
//         }
//     }
//     swap(&a[i], &a[pivot]);
// }

/**
 * Order values from a file, save values on a sorted file
 */
void orderFile(char *In, char *Out) {
    char tmpString[MAX_CHAR];
    FILE *File = smartFopen(In, "r");

    fgets(tmpString, MAX_CHAR, File);
    uint numberOfRows = atoi(tmpString);
    int values[numberOfRows];
    for (size_t i = 0; i < numberOfRows; i++) {
        fgets(tmpString, MAX_CHAR, File);
        values[i] = atoi(tmpString);
    }
    fclose(File);
    sorting(values, numberOfRows);

    File = smartFopen(Out, "w");
    for (size_t i = 0; i < numberOfRows; i++) {
        sprintf(tmpString, "%d\n", values[i]);
        fputs(tmpString, File);
    }
    fclose(File);
}

void orderFileWrapper(Parameter p) {
    printf("In: %s\tOut: %s\n", p->In, p->Out);
    orderFile(p->In, p->Out);
}

int main(int argc, char const *argv[]) {
    ARRAYS  = newArraysLib();
    Array a = parseParameters(argc, argv);

    pthread_t threads[a->ItemsNumber];
    for (size_t i = 0; i < a->ItemsNumber; i++) {
        if (MULTITHREAD) {
            pthread_create(&threads[i], NULL, &orderFileWrapper, a->Items[i]);
        } else {
            orderFileWrapper((Parameter)a->Items[i]);
        }
    }

    if (MULTITHREAD) {
        puts("Wait for therads to end");
        for (size_t i = 0; i < a->ItemsNumber; i++) {
            pthread_join(threads[i], NULL);
        }
        puts("Threads ended");
    }

    ARRAYS.free(a, true);
    return 0;
}
