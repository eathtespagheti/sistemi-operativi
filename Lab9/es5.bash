#!/usr/bin/env bash

[ -f "$1".out ] && rm "$1".out
maxLenght=0
lines=0
lenght=0

while IFS="" read -r line || [ -n "$line" ]; do
    lenght=$(echo "$line " | wc -w)
    [ "$lenght" -gt "$maxLenght" ] && maxLenght="$lenght"
    echo "$lenght" | tr '\n' ' ' >>"$1".out
    echo "$line" >>"$1".out
    lines=$((lines + 1))
done <"$1"

cat "$1".out
echo
echo "Longest line: $maxLenght"
echo "Number of lines $lines"
