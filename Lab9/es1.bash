#!/usr/bin/env bash

function searchRecursive() {
    [ -f "$1$2" ] && echo "$1$2"

    for directory in "$1"*/; do
        [ ! "$directory" = "$1*/" ] && {
            searchRecursive "$directory" "$2"
        }
    done
}

searchRecursive ./ "$1"

# find . -name "$1"