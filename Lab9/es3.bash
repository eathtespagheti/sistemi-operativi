#!/usr/bin/env bash

function checkConnection() {
    ping -c1 www.google.com
    return $((($? - 1) * -1))
}

if [ "$(checkConnection)" ]; then
    echo "Connected"
else
    echo "Not Connected"
fi
