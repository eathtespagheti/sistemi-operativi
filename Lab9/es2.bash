#!/usr/bin/env bash

function listUsers() {
    who | awk '{print $1}' | tr '\n' ' '
}

while true; do
    usersList=$(listUsers)
    if [[ "${usersList[*]}" =~ $1 ]]; then
        echo "$1 it's logged in"
    else
        echo "$1 it's NOT logged in"
    fi
    sleep "$2"s
done
