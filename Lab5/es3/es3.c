/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void child1() {
    int size = 50;
    char buff[size + 1];
    FILE *filestream = fopen("son1.txt", "r");
    fgets(buff, size, filestream);
    fclose(filestream);
    printf("%s\n", buff);
}

void child2() {
    int size = 50;
    char buff[size + 1];
    FILE *filestream = fopen("son2.txt", "r");
    fgets(buff, size, filestream);
    fclose(filestream);
    printf("%s\n", buff);
    sleep(5);
}

void sighandler(int sig) {
    printf("Received signal %d\n", sig);
    return;
}

int main(int argc, char const *argv[]) {
    sigset_t set;
    sigaddset(&set, SIGCHLD);
    signal(SIGCHLD, sighandler);

    int pid1 = fork();
    if (!pid1) {
        child1();
        _exit(0);
    }
    printf("Child 1 started with PID %d\n", pid1);
    pause();

    int pid2 = fork();
    if (!pid2) {
        child2();
        _exit(0);
    }
    printf("Child 2 started with PID %d\n", pid2);
    pause();

    return 0;
}
