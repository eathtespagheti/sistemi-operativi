#!/usr/bin/env sh
# Copyright (C) 2020 Fabio Sussarellu
#
# This file is part of sistemi-operativi.
#
# sistemi-operativi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sistemi-operativi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.

echo "Launching with es3"
./../../builds/es3 &
sleep 1s
pid="$!"
echo "Sending SIGCHLD to the process"
sudo kill -17 "$pid"
sleep 1s
echo "Sending SIGCONT to the process"
sudo kill -18 "$pid"
sleep 1s
echo "Sending SIGCHLD to the process"
sudo kill -17 "$pid"
sleep 1s
echo "Sending SIGCONT to the process"
sudo kill -18 "$pid"
sleep 1s
echo "Killing process"
sudo kill -9 "$pid"
