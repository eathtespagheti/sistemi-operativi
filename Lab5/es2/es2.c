/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void interrupt(int pid) {
    kill(pid, SIGINT);
}

void usr1(int pid) {
    kill(pid, SIGUSR1);
}

void usr2(int pid) {
    kill(pid, SIGUSR2);
}

int main(int argc, char const *argv[]) {
    int pid = atoi(argv[1]);

    char read[50];
    while (1) {
        printf("Type somma, differenza of fine: ");
        scanf("%s", read);
        if (strcmp(read, "somma") == 0) {
            usr2(pid);
        } else if (strcmp(read, "differenza") == 0) {
            usr1(pid);
        } else if (strcmp(read, "fine") == 0) {
            interrupt(pid);
            break;
        }
    }

    return 0;
}
