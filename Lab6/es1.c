/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#define CHAR_MAX 300

int WAIT = 1;

void sigHandler() {
    WAIT = 0;
}

int child(char *filename, int pipes[2]) {
    int pid      = getpid();
    int childpid = fork();

    if (!childpid) {
        FILE *toRead = fopen(filename, "r");
        char tempString[CHAR_MAX + 1];

        while (fgets(tempString, CHAR_MAX, toRead) != NULL) {
            write(pipes[1], tempString, CHAR_MAX);
            kill(pid, SIGCONT);
            kill(getpid(), SIGSTOP);
        }

        kill(pid, SIGCONT);
        kill(pid, SIGUSR1);
        fclose(toRead);
        _exit(0);
    }

    return childpid;
}

void stopmanager() {
    return;
}

int main(int argc, char const *argv[]) {
    char filename[] = "test";
    int pipes[2];
    pipe(pipes);
    int childpid = child(filename, pipes);
    signal(SIGCONT, stopmanager);

    char tempString[CHAR_MAX];
    signal(SIGUSR1, sigHandler);
    kill(getpid(), SIGSTOP);
    while (WAIT) {
        read(pipes[0], tempString, CHAR_MAX);
        printf("%s", tempString);
        kill(childpid, SIGCONT);
        kill(getpid(), SIGSTOP);
    }

    close(pipes[1]);
    close(pipes[0]);
    _exit(0);
}
