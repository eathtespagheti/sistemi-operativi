/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int N;
int childEnded = 0;

void execCalc(int *pipes, int i) {
    double result = pow(M_E, i);
    write(pipes[1], &result, sizeof(pipes));
    exit(0);
}

int *allocatePipes() {
    int *tmpPipe = calloc(2, sizeof(*tmpPipe));
    pipe(tmpPipe);
    return tmpPipe;
}

void freePipes(int *pipes) {
    if (pipes != NULL) {
        close(pipes[0]);
        close(pipes[1]);
        free(pipes);
    }
}

void childManager() {
    childEnded++;
    printf("Child %d ended\n", childEnded);
}

int main(int argc, char const *argv[]) {
    N = atoi(argv[1]);
    int pids[N];
    int *pipesCouples[N];
    signal(SIGCHLD, childManager);

    // For every worker
    for (size_t i = 0; i < N; i++) {
        pipesCouples[i] = allocatePipes();
        pids[i]         = fork();
        if (!pids[i]) { // Child process
            execCalc(pipesCouples[i], i + 1);
        }
    }

    while (childEnded < N) {
        printf("Waiting for childs, %d already exited\n", childEnded);
        pause();
    }

    double result = 0;
    double tmpValue;
    int *pipes;
    for (size_t i = 0; i < N; i++) {
        pipes = pipesCouples[i];
        read(pipes[0], &tmpValue, sizeof(tmpValue));
        freePipes(pipes);
        result += tmpValue;
    }

    printf("Result it's %f\n", result);

    return 0;
}
