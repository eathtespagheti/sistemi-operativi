#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#define BUF_SIZE 1024

void copyFile(char *srcFile, char *dstFile);

int main(int narg, char *argv[]) {
    if (narg != 3) {
        printf("Wrong inputs, Please specify two inputs\n");
        return 1;
    }
    char dstFile[500];
    char srcFile[500];
    strcpy(srcFile, argv[1]);
    strcpy(dstFile, argv[2]);
    copyFile(srcFile, dstFile);
    return 0;
}

void copyFile(char *srcFile, char *dstFile) {
    int inputFd;
    int outputFd;
    int openFlags;
    char buf[BUF_SIZE];
    mode_t filePerms;
    ssize_t numRead;
    inputFd = open(srcFile, O_RDONLY);
    if (inputFd == -1) {
        printf("opening file %s", srcFile);
    }
    openFlags = O_CREAT | O_WRONLY | O_TRUNC;
    filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    outputFd  = open(dstFile, openFlags, filePerms);
    if (outputFd == -1) {
        printf("opening file %s", dstFile);
    }
    while ((numRead = read(inputFd, buf, BUF_SIZE)) > 0) {
        printf("%d", numRead);
        if (write(outputFd, buf, numRead) != numRead)
            printf("couldn't write whole buffer");
        if (numRead == -1)
            printf("read");
        if (close(inputFd) == -1)
            printf("close input");
        if (close(outputFd) == -1)
            printf("close output");
    }
}