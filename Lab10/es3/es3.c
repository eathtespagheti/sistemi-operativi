/**
 * Copyright (C) 2021 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>

sem_t t1, t2;

void printID(int thread_number) {
    printf("Thread %d, id: %d\n", thread_number, pthread_self());
}

void thread1() {
    while (1) {
        sem_wait(&t1);
        printID(1);
        sleep(1);
        sem_post(&t2);
    }
};

void thread2() {
    while (1) {
        sem_wait(&t2);
        printID(2);
        sleep(1);
        sem_post(&t1);
    }
};

int main(int argc, char const *argv[]) {
    sem_init(&t1, 0, 1);
    sem_init(&t2, 0, 1);

    pthread_t pid1, pid2;
    pthread_create(&pid1, NULL, (void *)thread1, NULL);
    pthread_create(&pid2, NULL, (void *)thread2, NULL);
    pthread_join(pid1, NULL);
    pthread_join(pid2, NULL);

    sem_destroy(&t1);
    sem_destroy(&t2);
    return 0;
}
