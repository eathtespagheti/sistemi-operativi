/**
 * Copyright (C) 2021 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define maxSeconds 3
sem_t *sem_A, *sem_B, *sem_C, *sem_D, *sem_E, *sem_F, *sem_G, *sem_H, *sem_I;

void waitRandomTime() {
    int time = rand() % (maxSeconds + 1);
    printf("Waiting for %d seconds...\n", time);
    sleep(time);
}

void stepWork(char *name, int *run) {
    printf("\nStarting %s, run number %d\n", name, *run);
    *run = *run + 1;
    waitRandomTime();
    printf("Ending %s\n", name);
}

void stepA() {
    int runs = 0;
    while (1) {
        sem_wait(sem_A);
        stepWork("A", &runs);
        sem_post(sem_B);
        sem_post(sem_C);
        sem_post(sem_D);
    }
}

void stepB() {
    int runs = 0;
    while (1) {
        sem_wait(sem_B);
        stepWork("B", &runs);
        sem_post(sem_I);
    }
}

void stepC() {
    int runs = 0;
    while (1) {
        sem_wait(sem_C);
        stepWork("C", &runs);
        sem_post(sem_E);
        sem_post(sem_F);
    }
}

void stepD() {
    int runs = 0;
    while (1) {
        sem_wait(sem_D);
        stepWork("D", &runs);
        sem_post(sem_H);
    }
}

void threadE() {
    int runs = 0;
    while (1) {
        sem_wait(sem_E);
        stepWork("E", &runs);
        sem_post(sem_G);
    }
}

void threadF() {
    int runs = 0;
    while (1) {
        sem_wait(sem_F);
        stepWork("F", &runs);
        sem_post(sem_G);
    }
}

void threadG() {
    int runs = 0;
    while (1) {
        sem_wait(sem_G);
        sem_wait(sem_G);
        stepWork("G", &runs);
        sem_post(sem_I);
    }
}

void threadH() {
    int runs = 0;
    while (1) {
        sem_wait(sem_H);
        stepWork("H", &runs);
        sem_post(sem_I);
    }
}

void threadI() {
    int runs = 0;
    while (1) {
        sem_wait(sem_I);
        sem_wait(sem_I);
        sem_wait(sem_I);
        stepWork("I", &runs);
        sem_post(sem_A);
    }
}

int main(int argc, char const *argv[]) {
    // Semaphores initalization
    sem_A = sem_open("A", O_CREAT, 0600, 1);
    sem_B = sem_open("B", O_CREAT, 0600, 0);
    sem_C = sem_open("C", O_CREAT, 0600, 0);
    sem_D = sem_open("D", O_CREAT, 0600, 0);
    sem_I = sem_open("I", O_CREAT, 0600, 0);
    sem_E = sem_open("E", O_CREAT, 0600, 0);
    sem_F = sem_open("F", O_CREAT, 0600, 0);
    sem_G = sem_open("G", O_CREAT, 0600, 0);
    sem_H = sem_open("H", O_CREAT, 0600, 0);

    pthread_t threadAStatus;
    pthread_create(&threadAStatus, NULL, (void *)stepA, NULL);

    pid_t branch_B = fork();
    if (branch_B == 0) {
        int numberOfThreads = 2;
        pthread_t threads[numberOfThreads];

        pthread_create(&threads[0], NULL, (void *)stepB, NULL);
        pthread_create(&threads[1], NULL, (void *)threadI, NULL);

        // End threads
        for (size_t i = 0; i < numberOfThreads; i++) {
            pthread_join(threads[i], NULL);
            printf("Thread %d ended\n", i);
        }
    }

    pid_t branch_C = fork();
    if (branch_C == 0) {
        int numberOfThreads = 4;
        pthread_t threads[numberOfThreads];

        pthread_create(&threads[0], NULL, (void *)stepC, NULL);
        pthread_create(&threads[1], NULL, (void *)threadE, NULL);
        pthread_create(&threads[2], NULL, (void *)threadF, NULL);
        pthread_create(&threads[3], NULL, (void *)threadG, NULL);

        // End threads
        for (size_t i = 0; i < numberOfThreads; i++) {
            pthread_join(threads[i], NULL);
            printf("Thread %d ended\n", i);
        }
    }

    pid_t branch_D = fork();
    if (branch_D == 0) {
        int numberOfThreads = 2;
        pthread_t threads[numberOfThreads];

        pthread_create(&threads[0], NULL, (void *)stepD, NULL);
        pthread_create(&threads[1], NULL, (void *)threadH, NULL);

        // End threads
        for (size_t i = 0; i < numberOfThreads; i++) {
            pthread_join(threads[i], NULL);
            printf("Thread %d ended\n", i);
        }
    }

    pthread_join(threadAStatus, NULL);
    puts("Thread A ended");

    sem_destroy(sem_A);
    sem_destroy(sem_B);
    sem_destroy(sem_C);
    sem_destroy(sem_D);
    sem_destroy(sem_I);
    sem_destroy(sem_E);
    sem_destroy(sem_F);
    sem_destroy(sem_G);
    sem_destroy(sem_H);
    return 0;
}
