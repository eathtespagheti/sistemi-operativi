/**
 * Copyright (C) 2021 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

sem_t sem_BCD, sem_I, sem_EF, sem_H, sem_G;
unsigned int maxSeconds = 3;

void waitRandomTime() {
    int time = rand() % (maxSeconds + 1);
    printf("Waiting for %d seconds...\n", time);
    sleep(time);
}

void stepWork(char *name) {
    printf("\nStarting %s\n", name);
    waitRandomTime();
    printf("Ending %s\n", name);
}

void stepA() {
    stepWork("A");
    sem_post(&sem_BCD);
    sem_post(&sem_BCD);
    sem_post(&sem_BCD);
}

void stepB() {
    sem_wait(&sem_BCD);
    stepWork("B");
    sem_post(&sem_I);
}

void stepC() {
    sem_wait(&sem_BCD);
    stepWork("C");
    sem_post(&sem_EF);
    sem_post(&sem_EF);
}

void stepD() {
    sem_wait(&sem_BCD);
    stepWork("D");
    sem_post(&sem_H);
}

void threadE() {
    sem_wait(&sem_EF);
    stepWork("E");
    sem_post(&sem_G);
}

void threadF() {
    sem_wait(&sem_EF);
    stepWork("F");
    sem_post(&sem_G);
}

void threadG() {
    sem_wait(&sem_G);
    sem_wait(&sem_G);
    stepWork("G");
    sem_post(&sem_I);
}

void threadH() {
    sem_wait(&sem_H);
    stepWork("H");
    sem_post(&sem_I);
}

void threadI() {
    sem_wait(&sem_I);
    sem_wait(&sem_I);
    sem_wait(&sem_I);
    stepWork("I");
}

int main(int argc, char const *argv[]) {
    int numberOfThreads = 9;
    pthread_t threads[numberOfThreads];
    // Semaphores initalization
    sem_init(&sem_BCD, 0, 0);
    sem_init(&sem_I, 0, 0);
    sem_init(&sem_EF, 0, 0);
    sem_init(&sem_H, 0, 0);
    sem_init(&sem_G, 0, 0);

    // Launch threads
    puts("Launching threads");
    pthread_create(&threads[0], NULL, (void *)stepA, NULL);
    pthread_create(&threads[1], NULL, (void *)stepB, NULL);
    pthread_create(&threads[2], NULL, (void *)stepC, NULL);
    pthread_create(&threads[3], NULL, (void *)stepD, NULL);
    pthread_create(&threads[4], NULL, (void *)threadE, NULL);
    pthread_create(&threads[5], NULL, (void *)threadF, NULL);
    pthread_create(&threads[6], NULL, (void *)threadG, NULL);
    pthread_create(&threads[7], NULL, (void *)threadH, NULL);
    pthread_create(&threads[8], NULL, (void *)threadI, NULL);

    // End threads
    for (size_t i = 0; i < numberOfThreads; i++) {
        pthread_join(threads[i], NULL);
        printf("Thread %d ended\n", i);
    }

    sem_destroy(&sem_BCD);
    sem_destroy(&sem_I);
    sem_destroy(&sem_EF);
    sem_destroy(&sem_H);
    sem_destroy(&sem_G);
    return 0;
}