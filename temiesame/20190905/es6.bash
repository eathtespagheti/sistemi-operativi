#!/usr/bin/env bash

filename=$1
mapfile -t students < <(cut -d " " -f 1-3 "$filename" | sort | uniq)
courses=$( cut -d " " -f 4 "$filename" | sort | uniq )

echo "Media studenti:"
for student in "${students[@]}"; do
    exams=$(grep "$student" "$filename")
    votes=$(echo "$exams" | cut -d " " -f 5)

    sum=0
    n=0
    for vote in $votes; do
        [ "$vote" -ge 18 ] && {
            sum=$(("$sum" + "$vote"))
            n=$(("$n" + 1))
        }
    done
    avg=0
    [ "$n" -gt 0 ] && avg=$(echo "scale=1;$sum/$n" | bc -l)

    echo "  $student $avg"
done

echo "Media esami:"
for course in $courses; do
    exams=$(grep "$course" "$filename")
    votes=$(echo "$exams" | cut -d " " -f 5)

    sum=0
    n=0
    for vote in $votes; do
        sum=$(("$sum" + "$vote"))
        n=$(("$n" + 1))
    done
    avg=$(echo "scale=1;$sum/$n" | bc -l)

    echo "  $course $avg"
done
