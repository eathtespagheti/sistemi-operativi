#!/usr/bin/env bash

processes=$(ps -ef)

IFS=$'\n'
while read -r processName; do
    filtered=$(echo "$processes" | grep "$processName")
    for process in $filtered; do
        executableName=$(echo "$process" | awk '{print $8}')
        [ "$executableName" = "$processName" ] && {
            owner=$(echo "$process" | awk '{print $1}')
            parentpid=$(echo "$process" | awk '{print $3}')
            echo "Process $executableName run by $owner"
            echo "Child of $executableName's parent:"
            ps --ppid "$parentpid"
        }
    done
done <virus.dat
