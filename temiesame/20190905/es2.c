/**
 * Copyright (C) 2021 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void *t1(void *p) {
    pthread_t thread;
    int *pn = (int *)p;
    int n   = *pn;
    if (n > 0) {
        printf("thread: %d\n", n--);
        pthread_create(&thread, NULL, t1, &n);
    }
    pthread_join(thread, NULL);
    pthread_exit(NULL);
}
int main(int argc, char *argv[]) {
    pthread_t thread;
    int n = atoi(argv[1]);
    setbuf(stdout, 0);
    printf("main 1: %d\n", n);
    if (fork()) {
        printf("main 2: %d\n", -n);
        pthread_create(&thread, NULL, t1, &n);
        pthread_join(thread, NULL);
    } else {
        system("echo main 3: n\n");
        execlp("echo", "bash", "main 4:", "n", NULL);
    }
    return 1;
}