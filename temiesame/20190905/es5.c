#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#define R 4
#define C 4

typedef struct threadParameters {
    int *Row;
    int c;
    sem_t *toCheck;
    pthread_t TID;
} threadParameters;

void readRow(threadParameters *p) {
    sem_wait(p->toCheck);
    for (size_t i = 0; i < p->c; i++) {
        scanf("%d", &p->Row[i]);
    }
}

void writeRow(threadParameters *p) {
    sem_wait(p->toCheck);
    for (size_t i = 0; i < p->c; i++) {
        printf("%d\t", p->Row[i]);
    }
    puts("");
}

void readMatrix(int **M, int r, int c) {
    sem_t semaphores[r];
    threadParameters parameters[r];

    for (size_t i = 0; i < r; i++) {
        sem_init(&semaphores[i], 0, 0);
        parameters[i].c       = c;
        parameters[i].Row     = M[i];
        parameters[i].toCheck = &semaphores[i];
        pthread_create(&parameters[i].TID, NULL, (void *)readRow, &parameters[i]);
    }

    for (size_t i = 0; i < r; i++) {
        sem_post(&semaphores[i]);
        pthread_join(parameters[i].TID, NULL);
    }

    for (size_t i = 0; i < r; i++) {
        pthread_create(&parameters[i].TID, NULL, (void *)writeRow, &parameters[i]);
    }

    for (size_t i = 0; i < r; i++) {
        sem_post(&semaphores[i]);
        pthread_join(parameters[i].TID, NULL);
    }
}

void freeMatrix(int **M, int r) {
    for (size_t i = 0; i < r; i++) {
        free(M[i]);
    }
    free(M);
}

int **newMatrix(int r, int c) {
    int **m = calloc(r, sizeof(*m));
    for (size_t i = 0; i < r; i++) {
        m[i] = calloc(c, sizeof(**m));
    }
    return m;
}

int main(int argc, char const *argv[]) {
    int **M = newMatrix(R, C);

    readMatrix(M, R, C);

    freeMatrix(M, R);
    return 0;
}
