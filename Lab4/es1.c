/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

const int SLEEP_TIME = 2;
const int REPEAT     = 50;

void printRandomValue() {
    printf("%d\n", rand());
}

int main(int argc, char const *argv[]) {
    size_t child = 0;

    for (; child < 3; child++) {
        if (!fork()) {
            while (1) {
                int pid = fork();
                if (!pid) {
                    printf("Child number %d outs: ", child);
                    printRandomValue();
                    sleep(SLEEP_TIME);
                    exit(0);
                } else {
                    int status;
                    waitpid(pid, &status, 0);
                }
            }
        }
    }

    for (; child < 6; child++) {
        if (!fork()) {
            while (1) {
                int pid = fork();
                if (!pid) {
                    printf("Child number %d outs: ", child);
                    for (size_t i = 0; i < REPEAT; i++) {
                        printf("%d ", rand());
                    }
                    printf("\n");
                    sleep(SLEEP_TIME);
                    exit(0);
                } else {
                    int status;
                    waitpid(pid, &status, 0);
                }
            }
        }
    }

    while (wait(&child) != -1) {
        sleep(SLEEP_TIME);
    }

    return 0;
}
