/**
 * Copyright (C) 2020 Fabio Sussarellu
 * 
 * This file is part of sistemi-operativi.
 * 
 * sistemi-operativi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sistemi-operativi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int i, n, x;
    char str[50];

    n = atoi(argv[1]);
    x = atoi(argv[2]);

    printf("run with n=%d\n", n);
    fflush(stdout);

    for (i = 0; i < n; i++) {
        int pid = fork();
        if (pid > 0) {
            printf("%d\n", n - 1);
            sleep(x);

        } else if (pid == 0) {
            sprintf(str, "%s %d %s", argv[0], n - 1, argv[2]);
            system(str);
        } else {
            printf("Error creating child\n");
        }
    }
    exit(0);
}
